package com.ShteKen.endrem.world.structures;


import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.config.Config;
import com.mojang.serialization.Codec;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.SectionPos;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager;
import net.minecraft.world.gen.feature.structure.AbstractVillagePiece;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureStart;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraft.world.gen.feature.structure.*;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import org.apache.logging.log4j.Level;

public class EndGate extends Structure<NoFeatureConfig> {
    private final ResourceLocation START_POOL;
    private final int HEIGHT;
    private final int SPAWN_DISTANCE;

    public static final Biome.Category[] VALID_BIOMES = new Biome.Category[]{
            Biome.Category.PLAINS,
            Biome.Category.JUNGLE,
            Biome.Category.EXTREME_HILLS,
            Biome.Category.TAIGA,
            Biome.Category.MESA,
            Biome.Category.SAVANNA,
            Biome.Category.ICY,
            Biome.Category.DESERT,
            Biome.Category.SWAMP,
            Biome.Category.MUSHROOM,
            Biome.Category.FOREST
    };

    public EndGate(Codec<NoFeatureConfig> codec) {
        super(codec);
        this.START_POOL = new ResourceLocation(EndRemastered.MOD_ID, "end_gate/start_pool");
        this.HEIGHT = 15;
        this.SPAWN_DISTANCE = (int) (0.6 * Config.END_GATE_DISTANCE.get());
    }

    public static Boolean isValidSpawn(BiomeLoadingEvent event) {
        for (Biome.Category biomeCategory : VALID_BIOMES) {
            if (biomeCategory == event.getCategory()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public IStartFactory<NoFeatureConfig> getStartFactory() {
        return EndGate.Start::new;
    }

    @Override
    public GenerationStage.Decoration getDecorationStage() {
        return GenerationStage.Decoration.STRONGHOLDS;
    }

    @Override
    protected boolean func_230363_a_(ChunkGenerator chunkGenerator, BiomeProvider biomeSource, long seed, SharedSeedRandom chunkRandom, int chunkX, int chunkZ, Biome biome, ChunkPos chunkPos, NoFeatureConfig noFeatureConfig) {
        return getDistanceFromSpawn(chunkX, chunkZ) > this.SPAWN_DISTANCE;
    }

    protected int getDistanceFromSpawn(int chunkX, int chunkZ) {
        return (int) Math.sqrt(Math.pow(chunkX, 2) + Math.pow(chunkZ, 2)) << 4;
    }

    @Override
    public BlockPos func_236388_a_(IWorldReader world, StructureManager manager, BlockPos p_236388_3_, int radius, boolean skipExistingChunks, long seed, StructureSeparationSettings separationSettings) {
        int i = separationSettings.func_236668_a_();
        int j = p_236388_3_.getX() >> 4;
        int k = p_236388_3_.getZ() >> 4;
        int l = 0;

        for (SharedSeedRandom sharedseedrandom = new SharedSeedRandom(); l <= radius; ++l) {
            for (int i1 = -l; i1 <= l; ++i1) {
                boolean flag = i1 == -l || i1 == l;

                for (int j1 = -l; j1 <= l; ++j1) {
                    boolean flag1 = j1 == -l || j1 == l;
                    if (flag || flag1) {
                        int k1 = j + i * i1;
                        int l1 = k + i * j1;
                        ChunkPos chunkpos = this.getChunkPosForStructure(separationSettings, seed, sharedseedrandom, k1, l1);
                        IChunk ichunk = world.getChunk(chunkpos.x, chunkpos.z, ChunkStatus.STRUCTURE_STARTS);
                        StructureStart<?> structurestart = manager.getStructureStart(SectionPos.from(ichunk.getPos(), 0), this, ichunk);
                        if (structurestart != null && structurestart.isValid()) {
                            if (skipExistingChunks && structurestart.isRefCountBelowMax()) {
                                structurestart.incrementRefCount();
                                return new BlockPos(structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minX,
                                                    structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minY,
                                                    structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minZ);
                            }

                            if (!skipExistingChunks) {
                                return new BlockPos(structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minX,
                                        structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minY,
                                        structurestart.getComponents().get(((EndGate.Start) structurestart).getLocatedRoom()).getBoundingBox().minZ);
                            }
                        }

                        if (l == 0) {
                            break;
                        }
                    }
                }

                if (l == 0) {
                    break;
                }
            }
        }

        return null;
    }

    public class Start extends StructureStart<NoFeatureConfig> {
        public Start(Structure<NoFeatureConfig> structureIn, int chunkX, int chunkZ, MutableBoundingBox mutableBoundingBox, int referenceIn, long seedIn) {
            super(structureIn, chunkX, chunkZ, mutableBoundingBox, referenceIn, seedIn);
        }

        @Override
        public void func_230364_a_(DynamicRegistries dynamicRegistryManager, ChunkGenerator chunkGenerator, TemplateManager templateManagerIn, int chunkX, int chunkZ, Biome biomeIn, NoFeatureConfig config) {
            BlockPos genPosition = new BlockPos(chunkX << 4, HEIGHT, chunkZ << 4);

            JigsawManager.func_242837_a(
                    dynamicRegistryManager,
                    new VillageConfig(() -> dynamicRegistryManager.getRegistry(Registry.JIGSAW_POOL_KEY)
                            .getOrDefault(START_POOL),
                            Config.END_GATE_SIZE.get()),
                    AbstractVillagePiece::new,
                    chunkGenerator,
                    templateManagerIn,
                    genPosition,
                    this.components,
                    this.rand,
                    false,
                    false);

            this.recalculateStructureSize();
//            EndRemastered.LOGGER.log(Level.DEBUG, String.format("End Gate at %s", genPosition.getCoordinatesAsString()));
        }

        public int getLocatedRoom() {
            return Math.min(16, this.components.size()) - 1;
        }
    }
}
