package com.ShteKen.endrem.world.gen;

import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.util.RegistryHandler;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.BlockMatchRuleTest;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.ArrayList;

@Mod.EventBusSubscriber
public class OreSpawnHandler {

    private  static final ArrayList<ConfiguredFeature<?, ?>> netherOres = new ArrayList<ConfiguredFeature<?, ?>>();

    public static void registerOres(){
        netherOres.add(register("end_crystal_ore", Feature.ORE.withConfiguration(new OreFeatureConfig(new BlockMatchRuleTest(Blocks.BLACKSTONE),
                RegistryHandler.END_CRYSTAL_ORE.get().getDefaultState(), 3))
                .range(120).square() // spawn height
                .func_242731_b(15))); // spawn frequency per chunks

        netherOres.add(register("end_crystal_ore", Feature.ORE.withConfiguration(new OreFeatureConfig(new BlockMatchRuleTest(Blocks.POLISHED_BLACKSTONE_BRICKS),
                RegistryHandler.END_CRYSTAL_ORE.get().getDefaultState(), 3))
                .range(120).square() // spawn height
                .func_242731_b(25))); // spawn frequency per chunks

        netherOres.add(register("end_crystal_ore", Feature.ORE.withConfiguration(new OreFeatureConfig(new BlockMatchRuleTest(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS),
                RegistryHandler.END_CRYSTAL_ORE.get().getDefaultState(), 4))
                .range(120).square() // spawn height
                .func_242731_b(30))); // spawn frequency per chunks
    }


    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public static void gen(BiomeLoadingEvent event) {
        BiomeGenerationSettingsBuilder generation = event.getGeneration();
        if(event.getCategory().equals(Biome.Category.NETHER)){
            for(ConfiguredFeature<?, ?> ore : netherOres) {
                if(ore != null) generation.withFeature(GenerationStage.Decoration.UNDERGROUND_ORES, ore);
            }
        }
    }

    private static <FC extends IFeatureConfig>ConfiguredFeature<FC, ?> register(String name, ConfiguredFeature<FC, ?> configureFeature){
        return Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, new ResourceLocation(EndRemastered.MOD_ID + ":" + name), configureFeature);
    }
}
