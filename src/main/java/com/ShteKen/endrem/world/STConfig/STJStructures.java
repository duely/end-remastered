package com.ShteKen.endrem.world.STConfig;

import com.ShteKen.endrem.config.Config;
import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.world.structures.EndGate;
import net.minecraft.world.gen.feature.structure.Structure;
import com.google.common.collect.ImmutableMap;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class STJStructures {

    public static final DeferredRegister<Structure<?>> DEFERRED_REGISTRY_STRUCTURE = DeferredRegister.create(ForgeRegistries.STRUCTURE_FEATURES, EndRemastered.MOD_ID);


    public static final RegistryObject<Structure<NoFeatureConfig>> END_GATE = registerStructure("end_gate", () -> (new EndGate(NoFeatureConfig.field_236558_a_)));


    private static <T extends Structure<?>> RegistryObject<T> registerStructure(String name, Supplier<T> structure) {
        return DEFERRED_REGISTRY_STRUCTURE.register(name, structure);
    }


    public static void setupStructures() {
        setupMapSpacingAndLand(
                END_GATE.get(),
                new StructureSeparationSettings(
                        (Config.END_GATE_DISTANCE.get() + 20),
                        (Config.END_GATE_DISTANCE.get() - 20),
                        959834864),
                false);



        // Add more structures here
    }

    public static <F extends Structure<?>> void setupMapSpacingAndLand(
            F structure,
            StructureSeparationSettings structureSeparationSettings,
            boolean transformSurroundingLand)
    {
        Structure.NAME_STRUCTURE_BIMAP.put(structure.getRegistryName().toString(), structure);

        DimensionStructuresSettings.field_236191_b_ =
                ImmutableMap.<Structure<?>, StructureSeparationSettings>builder()
                        .putAll(DimensionStructuresSettings.field_236191_b_)
                        .put(structure, structureSeparationSettings)
                        .build();
    }
}
