package com.ShteKen.endrem.items;

import com.ShteKen.endrem.EndRemastered;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.text.*;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.entity.player.CriticalHitEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNullableByDefault;
import java.util.List;

@Mod.EventBusSubscriber
public class EndCrystalArmor extends ArmorItem {

    public EndCrystalArmor(EquipmentSlotType slot) {
        super(CustomArmorMaterial.END_CRYSTAL, slot, new Item.Properties().group(EndRemastered.TAB).rarity(Rarity.UNCOMMON));
    }


    @Nullable
    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
        int layer = slot == EquipmentSlotType.LEGS ? 2 : 1;

        if ("overlay".equals(type))
            return EndRemastered.MOD_ID + ":textures/models/armor/all_layer_" + layer + "_overlay.png";

        return EndRemastered.MOD_ID + ":textures/models/armor/end_crystal_layer_" + layer + ".png";
    }

    @ParametersAreNullableByDefault
    @OnlyIn(Dist.CLIENT)
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new TranslationTextComponent("item.endrem.armor.description.main"));

        if (Screen.hasShiftDown()){
            tooltip.add(new TranslationTextComponent("item.endrem.armor.description.shift"));
        }
        else {
            TranslationTextComponent shiftMessage = new TranslationTextComponent("item.endrem.press_shift");
            shiftMessage.setStyle(Style.EMPTY.setColor(Color.fromHex("#5454fc")));
            tooltip.add(shiftMessage);
        }
    }

    @SubscribeEvent
    public static void specialEffects(CriticalHitEvent event) {
        if (event.getDamageModifier() > 1F) {
            // Duration in ticks * MULTIPLIER
            int duration = 0;
            // Each second is 20 tick
            final int MULTIPLIER = 20;

            for (ItemStack stack : event.getPlayer().getArmorInventoryList()) {
                if (stack.getItem() instanceof EndCrystalArmor) {
                    duration++;
            }

            }
            duration *= MULTIPLIER;
            if (duration > 0 && event.getTarget().getType().getClassification() == EntityClassification.MONSTER) {
                event.getPlayer().addPotionEffect(new EffectInstance(Effects.REGENERATION, duration, 1));
            }
        }
        event.setResult(Event.Result.DEFAULT);
    }

    @Override
    public boolean makesPiglinsNeutral(ItemStack stack, LivingEntity wearer)
    {
        return stack.getItem() instanceof ArmorItem && ((ArmorItem) stack.getItem()).getArmorMaterial() == CustomArmorMaterial.END_CRYSTAL;
    }
}