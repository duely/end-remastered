package com.ShteKen.endrem.items;

import com.ShteKen.endrem.EndRemastered;
import net.minecraft.item.*;

public class EndCrystalTool{
    public final static IItemTier MATERIAL = CustomToolMaterial.END_CRYSTAL;

    public static class EndCrystalHoe extends HoeItem {
        public EndCrystalHoe(){
            super(MATERIAL, -4,0.0F, (new Item.Properties()).group(EndRemastered.TAB));
        }
    }

    public static class EndCrystalPickaxe extends PickaxeItem {
        public EndCrystalPickaxe(){
            super(MATERIAL, 1, -2.8F, (new Item.Properties()).group(EndRemastered.TAB));
        }
    }

    public static class EndCrystalAxe extends AxeItem {
        public EndCrystalAxe(){
            super(MATERIAL, 5.0F, -3.0F, (new Item.Properties()).group(EndRemastered.TAB));
        }
    }

    public static class EndCrystalSword extends SwordItem {
        public EndCrystalSword(){
            super(MATERIAL, 3, -2.4F, (new Item.Properties()).group(EndRemastered.TAB));
        }
    }

    public static class EndCrystalShovel extends ShovelItem {
        public EndCrystalShovel(){
            super(MATERIAL, 1.5F, -3.0F, (new Item.Properties()).group(EndRemastered.TAB));
        }
    }
}
