package com.ShteKen.endrem.blocks;

import com.ShteKen.endrem.util.RegistryHandler;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.entity.EntityType;
import net.minecraft.item.DyeColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.ToolType;

public class EndCrystalBlock extends AbstractGlassBlock {
   public EndCrystalBlock() {
        super(AbstractGlassBlock.Properties.create(Material.GLASS)
                .hardnessAndResistance(5f, 6f)
                .sound(SoundType.GLASS)
                .harvestTool(ToolType.PICKAXE)
                .harvestLevel(3)
                .setRequiresTool()
                .notSolid()
        );
    }
}
